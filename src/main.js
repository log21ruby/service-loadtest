import { sleep } from 'k6'
import { Rate } from 'k6/metrics'
import http from 'k6/http'

// let's collect all errors in one metric
let errorRate = new Rate('error_rate')

// See https://k6.io/docs/using-k6/options
export let options = {
  thresholds: {
    error_rate: ['rate < 0.1'],
  },
  stages: [
    { duration: '30s', target: 2000 },
    { duration: '30s', target: 3000 },
  ],
}

export default function() {
  let res = http.get('https://kong-lb.bitkub.com/test')

  errorRate.add(res.status >= 400)

  sleep(1)
}
